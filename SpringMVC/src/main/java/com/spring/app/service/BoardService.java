package com.spring.app.service;

import java.util.List;

import com.spring.app.vo.BoardVO;

public interface BoardService {
	public List<BoardVO> getAllBoard();

}
